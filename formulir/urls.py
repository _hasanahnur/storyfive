from django.contrib import admin
from django.urls import path, include
from .views import index, terima, delete, indexdelete, tampil, schedule_delete

urlpatterns = [
    path('', index, name="tambah"),
    path('terima/', terima),
    path('delete/', indexdelete, name="delete"),
    path('terimadelete/', delete),
    path('tampil/', tampil, name="tampil"),
    path('deletejadwal/<int:id>/', schedule_delete, name='schedule_delete'),
]
