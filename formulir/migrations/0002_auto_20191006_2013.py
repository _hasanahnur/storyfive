# Generated by Django 2.2.6 on 2019-10-06 13:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('formulir', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='simpanjadwal',
            name='date_time',
        ),
        migrations.AddField(
            model_name='simpanjadwal',
            name='fieldBulan',
            field=models.CharField(default='Januari', max_length=15),
        ),
        migrations.AddField(
            model_name='simpanjadwal',
            name='fieldHari',
            field=models.CharField(default='Senin', max_length=10),
        ),
        migrations.AddField(
            model_name='simpanjadwal',
            name='fieldKategori',
            field=models.CharField(default=' ', max_length=30),
        ),
        migrations.AddField(
            model_name='simpanjadwal',
            name='fieldNama',
            field=models.CharField(default=' ', max_length=20),
        ),
        migrations.AddField(
            model_name='simpanjadwal',
            name='fieldTahun',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='simpanjadwal',
            name='fieldTanggal',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='simpanjadwal',
            name='fieldTempat',
            field=models.CharField(default='', max_length=30),
        ),
    ]
