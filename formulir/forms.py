from django import forms
from .models import simpanJadwal

class jadwalForm(forms.Form):
    fieldDate = forms.DateField(
        label = 'Date',
        widget = forms.SelectDateWidget(
            years = range(1945,2019,1)
        )
    )
    fieldTime = forms.TimeField(
        label = 'Time',
        widget=forms.TimeInput(format='%H:%M')
    )
    fieldNama = forms.CharField(label = 'Nama Kegiatan')
    fieldTempat = forms.CharField(label = 'Tempat Kegiatan')
    fieldKategori = forms.CharField(label = 'Kategori Kegiatan')

class DeleteForm(forms.Form):
    fieldDelete = forms.CharField(label='Delete Jadwal:', max_length=30)
    

    

