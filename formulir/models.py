from django.db import models
from datetime import datetime

# Create your models here.

class simpanJadwal(models.Model):
    fieldDate = models.IntegerField(default = 0)
    fieldMonth = models.CharField(max_length = 20, default = " ")
    fieldYear = models.IntegerField(default = 0)
    fieldNama = models.CharField(max_length = 20, default = " ")
    fieldTempat = models.CharField(max_length = 30, default = "")
    fieldKategori = models.CharField(max_length = 30, default = " ")
