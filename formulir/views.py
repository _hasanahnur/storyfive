from django.shortcuts import render
from django.http import HttpResponse
from .forms import jadwalForm, DeleteForm
from .models import simpanJadwal

# Create your views here.

def index(request):
    formulir = jadwalForm()
    argument = {
        'generated_html' : formulir
    }
    return render(request, 'form.html', argument)

def indexdelete(request):
    formulir = DeleteForm()
    argument = {
        'generated_html' : formulir
    }
    return render(request, 'delete.html', argument)

def terima(request):
    date = request.POST['fieldDate_day']
    month = request.POST['fieldDate_month']
    year = request.POST['fieldDate_year']
    time = request.POST['fieldTime']
    nama = request.POST['fieldNama']
    tempat = request.POST['fieldTempat']
    kegiatan = request.POST['fieldKategori']

    monthdict={
        '1' : 'January',
        '2' : 'February',
        '3' : 'March',
        '4' : 'April',
        '5' : 'May',
        '6' : 'June',
        '7' : 'July',
        '8' : 'August',
        '9' : 'September',
        '10': 'November',
        '11': 'November',
        '12' : 'December',
    }

    tempJadwal = simpanJadwal(
        fieldDate = date,
        fieldMonth = monthdict[month],
        fieldYear = year,
        fieldNama = nama,
        fieldTempat = tempat,
        fieldKategori = kegiatan
    )
    tempJadwal.save()

    all = simpanJadwal.objects.all()
    argument = {
        'jadwal' : all
    }
    print(argument)
    return render(request, 'hasil.html', argument)

def tampil(request):
    all = simpanJadwal.objects.all()
    argument = {
        'jadwal' : all
    }
    print(argument)
    return render(request, 'hasil.html', argument)

def delete(request):
    delete = request.POST['fieldDelete']
    all = simpanJadwal.objects.all()

    for e in all:
        if e.fieldNama == delete:
            e.delete()
    
    all = simpanJadwal.objects.all()
    argument = {
        'jadwal' : all
    }
    print(argument)
    return render(request, 'hasil.html', argument)

def schedule_delete(request, id):
    simpanJadwal.objects.filter(id=id).delete()
    all = simpanJadwal.objects.all()
    argument = {
        'jadwal' : all
    }
    print(argument)
    return render(request, 'hasil.html', argument)

